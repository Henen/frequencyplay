package com.example.hf;

import android.app.Activity;
import android.app.Fragment;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		 private final int duration = 3; // seconds
		 private  int sampleRate = 8000;
		 private final int numSamples = duration * sampleRate;
		 private final double sample[] = new double[numSamples];
		 private  double freqOfTone = 100; // hz

		 private final byte generatedSnd[] = new byte[2 * numSamples];
		 Button startBtn;
		 Handler handler = new Handler();
		 EditText rate_edit;
		 EditText hz_edit;
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			rate_edit = (EditText)rootView.findViewById(R.id.EditText01);
			hz_edit = (EditText)rootView.findViewById(R.id.editText1);
			
			rate_edit.setText(""+sampleRate);
			hz_edit.setText(""+freqOfTone);
			
			
			
			
			
			
			
			
			startBtn = (Button)rootView.findViewById(R.id.button1);
			startBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					sampleRate = Integer.parseInt(rate_edit.getText().toString());
					freqOfTone = Double.parseDouble(hz_edit.getText().toString());
					Thread thread = new Thread(new Runnable() {
					      public void run() {
					        genTone();
					        handler.post(new Runnable() {
					    
					    public void run() {
					     playSound();
					    }
					   });
					      }   
					    });
					    thread.start();
					 }
				
			});
			
			
			
			
			
			
			return rootView;
			
			
			
		}

		@Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			  // Use a new tread as this can take a while
			Thread thread = new Thread(new Runnable() {
			      public void run() {
			        genTone();
			        handler.post(new Runnable() {
			    
			    public void run() {
			     playSound();
			    }
			   });
			      }   
			    });
			    thread.start();
			 }
		
		public void genTone(){
			  // fill out the array
			  for (int i = 0; i < numSamples; ++i) {
			   sample[i] = Math.sin(2 * Math.PI * i / (sampleRate/freqOfTone));
			  }

			  // convert to 16 bit pcm sound array
			  // assumes the sample buffer is normalised.
			  int idx = 0;
			  for (double dVal : sample) {
			   short val = (short) (dVal * 32767);
			   generatedSnd[idx++] = (byte) (val & 0x00ff);
			   generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
			  }
			 }

			 void playSound(){
			  AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
			    8000, AudioFormat.CHANNEL_CONFIGURATION_MONO,
			    AudioFormat.ENCODING_PCM_16BIT, numSamples,
			    AudioTrack.MODE_STATIC);
			  audioTrack.write(generatedSnd, 0, numSamples);
			  audioTrack.play();
			 }
	}

}
